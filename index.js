const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

//import routes
const authRoute = require("./routes/auth");
const userRoute = require("./routes/users");

const app = express();
dotenv.config();

//mongoose db
mongoose.connect(
  process.env.DB_PATH,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => console.log("DB connected")
);

//middleware
app.use(express.json());

//route middleware
app.use("/api/users", authRoute);
app.use("/api/users", userRoute);

//listen
app.listen(process.env.PORT, () => console.log("Server is up and running"));
