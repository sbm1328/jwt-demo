const router = require("express").Router();
const User = require("../model/User");
const verify = require("./verifyToken");

router.get("/", verify, async (req, res) => {
  const user = req.user;
  console.log("my user ", user);

  try {
    const users = await User.find();
    res.status(200).json({
      users,
      length: users.length,
    });
  } catch (err) {
    res.status(500).json({ err });
  }
});

module.exports = router;
