const router = require("express").Router();
const { regValidation, loginValidation } = require("../validation/auth");
const User = require("../model/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//register
router.post("/register", async (req, res) => {
  //validation
  const { error } = regValidation(req.body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  //email exists
  const emailExists = await User.findOne({ email: req.body.email });
  if (emailExists)
    return res.status(400).json({ message: "Email already exists" });

  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  //on success
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
  });
  try {
    const savedUser = await user.save();
    res.status(200).json({ message: `${user.name} successfully registered` });
  } catch (err) {
    res.status(500).json({ err });
  }
});

//login
router.post("/login", async (req, res) => {
  //validation
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  //email exists
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).json({ message: "Email is invalid" });

  //valid password
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).json({ message: "Passord is wrong" });

  //on success
  try {
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
    res
      .header("auth-token", token)
      .json({ message: "Succesfully logged in", token: token });
  } catch (err) {
    res.status(500).json({ err });
  }
});

module.exports = router;
